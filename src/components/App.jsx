import React, { Component } from "react";
import Pomodoro from "./Pomodoro";

import "./App.css";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      bgColor: "#000033"
    };

    this.updateBackground = this.updateBackground.bind(this);
  }

  render() {
    return (
      <div id="App" style={{ "background-color": this.state.bgColor }}>
        <header>
          <h1>Pomodoro Clock</h1>
          <a href="https://www.freecodecamp.org" target="_blank" rel="noopener noreferrer">FreeCodeCamp.org</a>
        </header>
        <main>
          <Pomodoro onColorChange={this.updateBackground} />
        </main>
        <footer>
          <p>Coded by <a href="https://www.freecodecamp.org/elrolfe" target="_blank" rel="noopener noreferrer">Eric Rolfe</a></p>
          <p>
            Audio file:&nbsp;
            <a href="http://soundbible.com/1599-Store-Door-Chime.html">"Store Door Chime"</a>&nbsp;
            by Mike Koenig.&nbsp;
            <a href="https://creativecommons.org/licenses/by/3.0/us/legalcode" target="_blank" rel="noopener noreferrer">CC BY 3.0 US</a>
          </p>
        </footer>
      </div>
    );
  }

  updateBackground(bgColor) {
    this.setState({
      bgColor
    });
  }
}

export default App;
