import React, { Component } from "react";

import "./Clock.css";

const FULL_CIRCLE = Math.PI * 2;
const QUARTER_CIRCLE = Math.PI / 2;

class Clock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      context: null
    };
  }

  componentDidMount() {
    const context = document.getElementById("timer").getContext("2d");
    context.canvas.height = context.canvas.width;
    this.draw(context);
    this.setState({
      context
    });
  }

  componentDidUpdate() {
    this.draw();
  }

  componentWillUnmount() {
    this.setState({
      context: null
    });
  }

  draw(ctx = null) {
    const context = ctx || this.state.context;
    const [x, y] = [context.canvas.width / 2, context.canvas.height / 2];
    const radius = x * 0.85;
    const lineWidth = x * 0.1;
    const percentage = Math.max(0, Math.min(this.props.percentage, 1));

    context.beginPath();
    context.arc(x, y, radius, 0, FULL_CIRCLE, false);
    context.lineWidth = lineWidth;
    context.strokeStyle = "#888888";
    context.stroke();

    context.beginPath();
    context.arc(x, y, radius, -QUARTER_CIRCLE, -QUARTER_CIRCLE + (FULL_CIRCLE * percentage), false);
    context.lineWidth = x * 0.06;
    context.strokeStyle = "#ffffff";
    context.stroke();
  }

  render() {
    return (
      <div className="clock">
        <canvas id="timer"></canvas>
        <div className="labels">
          <p id="timer-label">{this.props.label}</p>
          <p id="time-left">{this.props.timeLeft}</p>
        </div>
      </div>
    );
  }
}

export default Clock;
