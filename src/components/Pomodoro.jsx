import React, { Component } from "react";
import { FontAwesomeIcon as FAIcon } from "@fortawesome/react-fontawesome";
import { faPause, faPlay, faUndoAlt } from "@fortawesome/free-solid-svg-icons";
import Clock from "./Clock";
import ClockControl from "./ClockControl";

import beep from "../assets/chime.mp3";

import "./Pomodoro.css";

const STATE_RESET = "STATE_RESET";
const STATE_PAUSE = "STATE_PAUSE";
const STATE_SESSION = "STATE_SESSION";
const STATE_BREAK = "STATE_BREAK";

const COLORS = {
  STATE_RESET: "#000066",
  STATE_SESSION: "#006600",
  STATE_BREAK: "#660000",
  STATE_PAUSE: "#555555"
};

const DEFAULT_STATE = {
  breakLength: 5,
  clockTimeLeft: "25:00",
  displayOffsetTime: 0,
  remainingTime: 1500,
  previousState: STATE_SESSION,
  sessionLength: 25,
  state: STATE_RESET,
  timerClockDisplay: null,
  timerCountdown: null,
  totalTime: 1500,
}

class Pomodoro extends Component {
  constructor(props) {
    super(props);

    this.state = Object.assign({}, DEFAULT_STATE);

    this.reset = this.reset.bind(this);
    this.setDisplayOffset = this.setDisplayOffset.bind(this);
    this.setRemainingTime = this.setRemainingTime.bind(this);
    this.startStop = this.startStop.bind(this);
    this.updateBreakLength = this.updateBreakLength.bind(this);
    this.updateSessionLength = this.updateSessionLength.bind(this);
  }

  clockLabel(state) {
    switch (state) {
      case STATE_PAUSE: return "Paused";
      case STATE_SESSION: return "Session";
      case STATE_BREAK: return "Break";
      case STATE_RESET:
      default: return "Pomodoro"
    }
  }

  componentDidMount() {
    this.props.onColorChange(COLORS.STATE_RESET);
  }

  render() {
    const percent =
      (this.state.totalTime - (this.state.remainingTime - this.state.displayOffsetTime)) / this.state.totalTime;

    return(
      <div className="pomodoro">
        <div className="controls">
          <ClockControl
            name="Session"
            timerLength={this.state.sessionLength}
            disabled={this.state.state === STATE_SESSION || this.state.state === STATE_BREAK}
            highlighted={this.state.state === STATE_SESSION}
            onUpdate={this.updateSessionLength}
          />
          <ClockControl
            name="Break"
            timerLength={this.state.breakLength}
            disabled={this.state.state === STATE_SESSION || this.state.state === STATE_BREAK}
            highlighted={this.state.state === STATE_BREAK}
            onUpdate={this.updateBreakLength}
          />
        </div>
        <Clock
          label={this.clockLabel(this.state.state)}
          timeLeft={this.state.clockTimeLeft}
          percentage={percent}
        />
        <div className="controls buttons">
          <button
            id="reset"
            onClick={this.reset}
          >
            <FAIcon icon={faUndoAlt} />
          </button>
          <button
            id="start_stop"
            onClick={this.startStop}
          >
            <FAIcon icon={this.startStopIcon()} />
          </button>
        </div>
        <audio id="beep">
          <source src={beep} type="audio/mpeg" />
        </audio>
      </div>
    );
  }

  reset() {
    document.getElementById("beep").pause();
    document.getElementById("beep").currentTime = 0;
    clearInterval(this.state.timerClockDisplay);
    clearInterval(this.state.timerCountdown);
    this.setState(DEFAULT_STATE);
    this.props.onColorChange(COLORS.STATE_RESET);
  }

  setDisplayOffset() {
    this.setState({
      displayOffsetTime: this.state.displayOffsetTime + 1 / 30
    });
  }

  setRemainingTime() {
    if (this.state.remainingTime) {
      const remainingTime = this.state.remainingTime - 1;
      const clockTimeLeft = `${
        Math.floor(remainingTime / 60).toString().padStart(2, "0")
      }:${
        (remainingTime % 60).toString().padStart(2, "0")
      }`;

      this.setState({
        clockTimeLeft,
        displayOffsetTime: 0,
        remainingTime
      });
    } else {
      document.getElementById("beep").play();

      const length =
        this.state.state === STATE_SESSION
          ? this.state.breakLength
          : this.state.sessionLength;

      this.setState({
        clockTimeLeft: `${length.toString().padStart(2, "0")}:00`,
        displayOffsetTime: 0,
        remainingTime: length * 60,
        state: this.state.state === STATE_SESSION ? STATE_BREAK : STATE_SESSION,
        totalTime: length * 60
      });
      this.props.onColorChange(COLORS[this.state.state === STATE_SESSION ? STATE_SESSION : STATE_BREAK]);
    }
  }

  startStop() {
    switch (this.state.state) {
      case STATE_RESET:
      case STATE_PAUSE:
        this.setState({
          state: this.state.previousState,
          timerClockDisplay: setInterval(this.setDisplayOffset, 33),
          timerCountdown: setInterval(this.setRemainingTime, 1000)
        });
        this.props.onColorChange(COLORS[this.state.previousState]);
      break;

      case STATE_SESSION:
      case STATE_BREAK:
        clearInterval(this.state.timerClockDisplay);
        clearInterval(this.state.timerCountdown);
        this.setState({
          state: STATE_PAUSE,
          previousState: this.state.state,
          timerClockDisplay: null,
          timerCountdown: null
        });
        this.props.onColorChange(COLORS.STATE_PAUSE);
      break;

      default:
      break;
    }
  }

  startStopIcon() {
    return this.state.state === STATE_RESET || this.state.state === STATE_PAUSE ? faPlay : faPause;
  }

  updateBreakLength(value) {
    this.setState({
      breakLength: value,
      clockTimeLeft: `${this.state.sessionLength.toString().padStart(2, "0")}:00`,
      displayOffsetTime: 0,
      previousState: STATE_SESSION,
      remainingTime: this.state.sessionLength * 60,
      state: STATE_RESET,
      totalTime: this.state.sessionLength * 60
    });
    this.props.onColorChange(COLORS.STATE_RESET);
  }

  updateSessionLength(value) {
    this.setState({
      clockTimeLeft: `${value.toString().padStart(2, "0")}:00`,
      displayOffsetTime: 0,
      previousState: STATE_SESSION,
      remainingTime: value * 60,
      sessionLength: value,
      state: STATE_RESET,
      totalTime: value * 60
    });
    this.props.onColorChange(COLORS.STATE_RESET);
  }
}

export default Pomodoro;
