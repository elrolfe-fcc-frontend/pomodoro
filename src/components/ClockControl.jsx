import React, { Component } from "react";
import { FontAwesomeIcon as FAIcon } from "@fortawesome/react-fontawesome";
import { faAngleDoubleDown, faAngleDoubleUp } from "@fortawesome/free-solid-svg-icons";

import "./ClockControl.css";

class ClockControl extends Component {
  constructor(props) {
    super(props);

    this.decrement = this.decrement.bind(this);
    this.increment = this.increment.bind(this);
  }

  decrement() {
    this.props.onUpdate(Math.max(1, Math.min(this.props.timerLength - 1, 60)));
  }

  increment() {
    this.props.onUpdate(Math.max(1, Math.min(this.props.timerLength + 1, 60)));
  }

  render() {
    const name = this.props.name.toLowerCase();
    const capitalizedName = name.substring(0, 1).toUpperCase() + name.substring(1);
    const timerLength = Math.max(1, Math.min(this.props.timerLength, 60));
    const clockControlClass = `clock-control${this.props.disabled ? " disabled" : ""}`;
    const labelClass = `label${this.props.highlighted ? " highlighted" : ""}`;

    return (
      <div className={clockControlClass}>
        <p className={labelClass} id={`${name}-label`}>{capitalizedName} Length</p>
        <p className="sublabel">in Minutes</p>
        <div className="controls">
          <button
            id={`${name}-decrement`}
            disabled={timerLength === 1 || this.props.disabled}
            onClick={this.decrement}
          >
            <FAIcon icon={faAngleDoubleDown} />
          </button>
          <p className="length" id={`${name}-length`}>{timerLength}</p>
          <button
            id={`${name}-increment`}
            disabled={timerLength === 60 || this.props.disabled}
            onClick={this.increment}
          >
            <FAIcon icon={faAngleDoubleUp} />
          </button>
        </div>
      </div>
    )
  }
}

export default ClockControl;
